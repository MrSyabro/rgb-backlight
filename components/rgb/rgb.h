#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include "esp_system.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/pwm.h"

#ifndef RGB_H
#define RGB_H

#define PWM_PERIOD	1000
#define R_PIN				15
#define G_PIN				14
#define B_PIN				12

#define rgb_new_color_t() malloc(sizeof(rgb_color_t))
#define rgb_new_task_t() malloc(sizeof(rgb_task_t))

typedef struct {
	int32_t r;
	int32_t g;
	int32_t b;
} rgb_color_t;

typedef struct {
	rgb_color_t *color_s;
	uint32_t steps;
} rgb_task_t;

typedef struct {
	rgb_task_t* tasklist[8];
	int8_t n;
	//bool clear[16];
}	rgb_tasklist_t;

void rgb_init();
void rgb_set_color(rgb_color_t *color);
void rgb_add_task(rgb_task_t *new_task);

#endif
