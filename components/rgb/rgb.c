#include "rgb.h"

static const char *TAG = "RGB_DRIVER";

static rgb_tasklist_t *rgb_tasklist;
static rgb_color_t *current_color;
bool rgb_stoptask = 0;

const uint32_t pin_num[3] = {R_PIN, G_PIN, B_PIN};

// duties table, real_duty = duties[x]/PERIOD
uint32_t duties[3] = {0, 0, 0};

// phase table, delay = (phase[x]/360)*PERIOD
int16_t phases[3] = {0, 0, 0};

void rgb_run_task(rgb_task_t *task)
{
	ESP_LOGI(TAG, "Run task! Steps: %d", task->steps);
	for (uint32_t step = 0; step < task->steps; step++){
		current_color->r = current_color->r + task->color_s->r;
		current_color->g = current_color->g + task->color_s->g;
		current_color->b = current_color->b + task->color_s->b;
		pwm_set_duty (0, current_color->r);
		pwm_set_duty (1, current_color->g);
		pwm_set_duty (2, current_color->b);
    	pwm_start();
		vTaskDelay (10 / portTICK_RATE_MS);
	}
}

void rgb_taskmanager (void *parm)
{
	while (!rgb_stoptask)
	{
		if (rgb_tasklist->n > -1)
		{
			for (uint16_t task_number = 0; task_number < rgb_tasklist->n; task_number++)
			{
				rgb_task_t *current_task = rgb_tasklist->tasklist[task_number];
				rgb_run_task (current_task);
				free(current_task);
			}
			rgb_tasklist->n = -1;
		}
		vTaskDelay(10/portTICK_RATE_MS);
	}

	vTaskDelete(NULL);
}

void rgb_init()
{
	pwm_init(PWM_PERIOD, duties, 3, pin_num);
	pwm_set_phases(phases);
	pwm_start();

	//rgb_tasklist = malloc(sizeof(rgb_tasklist_t));
	//rgb_tasklist->n = -1;
	current_color = rgb_new_color_t ();
  	current_color->r = 0;
  	current_color->g = 0;
  	current_color->b = 0;

	//ESP_LOGI(TAG, "Start taskmanager...");
	//xTaskCreate(rgb_taskmanager, "rgb_taskmanager", 4096, NULL, 5, NULL);
}

void rgb_set_color(rgb_color_t *new_color)
{
	ESP_LOGI(TAG, "Current color: r=%d, g=%d, b=%d", current_color->r, current_color->g, current_color->b);
  	ESP_LOGI(TAG, "Next color: r=%d, g=%d, b=%d", new_color->r*4, new_color->g*4, new_color->b*4);
	rgb_color_t *color = rgb_new_color_t ();
  	color->r = (new_color->r*4 - current_color->r)/100;
  	color->g = (new_color->g*4 - current_color->g)/100;
  	color->b = (new_color->b*4 - current_color->b)/100;
  	ESP_LOGI(TAG, "Color step: r=%d, g=%d, b=%d", color->r, color->g, color->b);

  	rgb_task_t *new_task = rgb_new_color_t ();
	new_task->color_s = color;
	new_task->steps = 100;
	rgb_add_task (new_task);
}

void rgb_add_task(rgb_task_t *new_task)
{
	//rgb_tasklist->tasklist[rgb_tasklist->n + 1] = new_task;
	//rgb_tasklist->n++;
	//ESP_LOGI(TAG, "Added task number %d", rgb_tasklist->n);
	rgb_run_task (new_task);
	free(new_task);
}
